#!/usr/bin/python
# -*- coding: utf8 -*-

import logging
import os
import random
import sys
from time import sleep, time

from colored import attr, bg, fg
from pynput import keyboard
from reprint import output

logging.basicConfig(filename='debug.log', level=logging.DEBUG)
os.system('chcp 65001')

h = 24
w = 56
top = 8
left = 8
viewbox = []
mMap = {}
framestart = time()
ticktime = .05  # 20fps


dPad = {
    "up": False,
    "down": False,
    "left": False,
    "right": False
}

controlD = None
altKey = False
itsDead = False


resolutions = [
    {'h': 40, 'w': 18},
    {'h': 56, 'w': 24},
    {'h': 72, 'w': 36}
]


def setres(which):
  global display_char
  display_char = whichg
  global w
  global h
  n = int(which)
  w = resolutions[n]['w']
  h = resolutions[n]['h']


def play():
  global display_char
  display_char = 'P'


class UIElement(object):
  def __init__(self, x, y, w, h, title, text, options, id):
    self.id = id
    self.posX = x
    self.posY = y
    self.width = w
    self.height = h
    self.title = title
    self.textContent = text
    self.options = options

  def selectPrev(self):
    for o, option in enumerate(self.options):
      if option['selected']:
        if o > 0:
          self.options[o - 1]['selected'] = True
        else:
          self.options[len(self.options) - 1]['selected'] = True

        self.options[o]['selected'] = False
        break

  def selectNext(self):
    for o, option in enumerate(self.options):

      if option['selected']:
        if o + 1 < len(self.options):
          self.options[o + 1]['selected'] = True
        else:
          self.options[0]['selected'] = True

        self.options[o]['selected'] = False
        break

  def selectNone(self):
    global uiView
    self.options[uiView.choice]['selected'] = True
    uiView.choice = None
    self.selectPrev()
    self.selectNext()

  def getSelected(self):
    for o, option in enumerate(self.options):
      if option['selected']:
        return option


nbsp = ' '


class UIView(object):
  def __init__(self):
    self.borders = {
        'topLeft': '┏',
        'topRight': '┓',
        'bottomLeft': '┗',
        'bottomRight': '┛',
        'vertEdge': '┃',
        'horizEdge': '━',
        'horizShadow': '▀'
    }
    self.elements = []
    self.display = []
    self.choice = None

    mainTitle = "Clever Name"
    mainContentText = 'something something\n FOO BAR BAZ'
    howWide = 41
    howHigh = 14
    offsetX = int((w / 2) - (howWide / 2))
    offsetY = int((h / 2) - (howHigh / 2)) - 1
    mainTitle = mainTitle.replace(' ', nbsp)
    mainContentText = mainContentText.replace(' ', nbsp)

    mainDialog = UIElement(
        offsetX, offsetY, howWide, howHigh,
        mainTitle, mainContentText,
        [
            {'text': 'PLAY', 'link': '_function_play(1)', 'selected': True},
            {'text': 'OPTIONS', 'link': 'settings_0', 'selected': False},
            {'text': 'EDITOR', 'link': 'editor_0', 'selected': False},
            {'text': '?', 'link': 'about_0', 'selected': False}
        ], 'main_0'
    )
    self.elements.append(mainDialog)
    self.activeElement = mainDialog

    optionsTitle = "OPTIONS"
    contentText1 = 'Resolution:'
    howWide = 32
    howHigh = 16
    offsetX = int((w / 2) - (howWide / 2))
    offsetY = int((h / 2) - (howHigh / 2)) - 1

    optionsDialog = UIElement(
        offsetX, offsetY, howWide, howHigh,
        optionsTitle, contentText1,
        [
            {'text': 'Lowest (40 x 18)',
             'link': '_function_setres(0)', 'selected': False},
            {'text': 'Lower (56 x 24)',
             'link': '_function_setres(1)', 'selected': True},
            {'text': 'Low (80 x 36)',
             'link': '_function_setres(2)', 'selected': False}
        ], 'settings_0'
    )
    self.elements.append(optionsDialog)

  def intendSelected(self):
    global display_char
    display_char = '^'
    self.choice = self.activeElement.getSelected()

  def hitSelected(self):
    global display_char
    global uiView
    global show_ui
    if self.choice != None:
      link = self.choice['link']
      if link[0] == '_':

        if link[1:].split('_')[0] == 'function':
          display_char = 'f'
          theFunction = link[1:].split('_')[1].split('(')[1].split(')')[0]
          arg = link.split('(')[1].split(')')[0]
          if theFunction == 'setres':
            display_char = 's'
            setres(arg)
          elif link.find('_play') > -1:
            display_char = 'p'
            self.activeElement = None
            self.show_ui = False
            play()
      else:
        display_char = "Y"
        for e, el in enumerate(uiView.elements):
          if el.id == link:
            display_char = str(e)
            self.activeElement = el

      self.choice = None

  def render(self):
    global w
    global h
    self.display = []
    for py in range(h + 1):
      self.display.append([])
      for px in range(w + 1):
        self.display[py].append(' ')  # clear everything

    element = self.activeElement
    if element == None:
      return 0
    startX = element.posX
    startY = element.posY
    endX = element.posX + element.width
    endY = element.posY + element.height
    self.display[startY - 1][startX - 1] = self.borders['topLeft']  # corners
    self.display[startY - 1][endX + 1] = self.borders['topRight']
    self.display[endY + 1][startX - 1] = self.borders['bottomLeft']
    self.display[endY + 1][endX + 1] = self.borders['bottomRight']

    for x in range(startX, endX + 1):
      self.display[startY - 1][x] = self.borders['horizEdge']  # edges
      self.display[endY + 1][x] = self.borders['horizEdge']
      self.display[endY + 2][x] = self.borders['horizShadow']

    for y in range(startY, endY + 1):
      self.display[y][startX - 1] = self.borders['vertEdge']  # edges
      self.display[y][endX + 1] = self.borders['vertEdge']  # edges

    for y in range(startY, endY + 1):  # bg
      for x in range(startX, endX + 1):
        self.display[y][x] = nbsp

    tw = len(element.title)
    halfw = w / 2
    tstart = int(halfw - (tw / 2))
    for x in range(0, tw):
      if tw > x:
        self.display[startY + 1][tstart + x] = element.title[x]  # title

    textLines = element.textContent.split('\n')
    longest = 0
    for l, line in enumerate(textLines):
      if len(line) > longest:
        longest = len(line)
    tw = longest
    tstart = int(halfw - (tw / 2))
    for l, line in enumerate(textLines):
      for x in range(0, tw):
        if len(line) > x:
          self.display[startY + 3 + l][tstart + x] = line[x]  # body text

    startY = startY + 4 + len(textLines)
    bulletPt = "❊"
    for o, option in enumerate(element.options):  # selectable options
      optionText = str(option['text'])
      isSelected = option['selected']
      yPos = startY + (2 * o)
      endChr = attr('reset')
      if isSelected:
        optionText = '[ ' + optionText + ' ]'

      else:
        colorChr = endChr
        endChr = ''
        optionText = ' {' + optionText + '} '

      if uiView.choice is option:
        self.display[yPos][tstart - 1] = bulletPt
      optionText = optionText.replace(' ', nbsp)
      strlen = len(optionText)
      for x in range(0, strlen):
        xPos = tstart + (1 + x)
        if x == 0:
          renderedChar = optionText[x]
        elif x > -1 and x < strlen - 1:
          renderedChar = optionText[x]
        else:
          renderedChar = optionText[x]

        self.display[yPos][xPos] = renderedChar

    return self.display


uiView = UIView()
uiView.show_ui = True


def log(txt):
  logging.info(txt)
  return True


def hidecursor():
  # if not (os.name == 'nt'):
  os.system('stty -echo')
  os.system('tput civis')





def newmap(w, h):
  grid = []
  stuff = []
  player = {
      'name':'player',
      'pos':{'x':0,'y':0}
  }
  stuff.append(player)
  for x in range(w):
    col = []
    for y in range(h):
      col.append(random.choice(['X', '0', '.']))
    grid.append(col)

  return {'tiles':grid, 'entities':stuff}


def crop(amap, topleft):
  croppedY = amap[topleft[1]:topleft[1] + h]
  croppedXY = []
  for rows in croppedY:
    croppedXY.append(rows[topleft[0]:topleft[0] + w])
  return croppedXY


def zap(count):
  sys.stdout.write("\033[K")
  sys.stdout.write("\n")
  sys.stdout.write("\033[F")
  sys.stdout.write("\033[K")
  for line in range(count):
    sys.stdout.write("\033[F")
  sys.stdout.write("\033[K")


def init():
  sys.stdout = open(sys.stdout.fileno(), mode='w',
                    encoding='utf8', buffering=1)
  hidecursor()
  global mMap
  global display_char
  display_char = '0'
  mMap = newmap(100, 100)


def on_press(key):
  global dPad
  global controlD
  global display_char
  global altKey
  global ticktime
  global uiView
  if ("KeyCode" in str(type(key))) and key.char:
    # is alphanumeric
    display_char = "o"

  else:
    if key == keyboard.Key.alt_r or key == keyboard.Key.alt_l or key == keyboard.Key.alt:
      display_char = 'a'
      altKey = True

    if key == keyboard.Key.left:
      dPad['left'] = True
    if key == keyboard.Key.right:
      uiView.intendSelected()
      dPad['right'] = True
    if key == keyboard.Key.up:
      dPad['up'] = True
    if key == keyboard.Key.down:
      dPad['down'] = True

  controlD = calc_dpad_input(dPad)


def toArrow(compassDir):
  arrowChar = '0'
  if compassDir == 'W':
    arrowChar = '1'
  elif compassDir == 'NW':
    arrowChar = '2'
  elif compassDir == 'N':
    arrowChar = '3'
  elif compassDir == 'NE':
    arrowChar = '4'
  elif compassDir == 'E':
    arrowChar = '5'
  elif compassDir == 'SE':
    arrowChar = '6'
  elif compassDir == 'S':
    arrowChar = '7'
  elif compassDir == 'SW':
    arrowChar = '8'
  return arrowChar


def calc_dpad_input(dPad):
  dCommand = None
  if dPad['down']:
    if dPad['left']:
      dCommand = 'SW'
    elif dPad['right']:
      dCommand = 'SE'
    else:
      dCommand = 'S'

  if dPad['up']:
    if dPad['left']:
      dCommand = 'NW'
    elif dPad['right']:
      dCommand = 'NE'
    else:
      dCommand = 'N'

  if dPad['left']:
    if dPad['up']:
      dCommand = 'NW'
    elif dPad['down']:
      dCommand = 'SW'
    else:
      dCommand = 'W'

  if dPad['right']:
    if dPad['up']:
      dCommand = 'NE'
    elif dPad['down']:
      dCommand = 'SE'
    else:
      dCommand = 'E'

  if not (dPad['right'] or dPad['left']) and not (dPad['up'] or dPad['down']):
    dCommand = None

  return dCommand


def on_release(key):
  global display_char
  global controlD
  global dPad
  global uiView
  global ticktime
  global altKey

  if key == keyboard.Key.alt_l or key == keyboard.Key.alt_r or key == keyboard.Key.alt:
    altKey = False
    display_char = 'x'

  elif ("KeyCode" in str(type(key))):
    # is alphanumeric
    display_char = '9'
    if key == keyboard.KeyCode.from_char('`') or key == keyboard.KeyCode.from_char('~'):
      if uiView.show_ui:
        uiView.show_ui = False
      else:
        uiView.show_ui = True
    elif altKey and key == keyboard.KeyCode.from_char('q'):
      itsDead = True
      display_char = 'Q'
      listener.stop()
      os.system('stty echo')
      os.system('tput cnorm')
      os._exit(0)

    # is 'special' key

  controlD == None

  if key == keyboard.Key.left:
    if uiView.show_ui:
      uiView.activeElement = uiView.elements[0]
    dPad['left'] = False

  if key == keyboard.Key.right:
    if uiView.show_ui:
      uiView.hitSelected()
    dPad['right'] = False

  if key == keyboard.Key.up:
    if uiView.show_ui:
      uiView.activeElement.selectPrev()
    dPad['up'] = False

  if key == keyboard.Key.down:
    if uiView.show_ui:
      uiView.activeElement.selectNext()
    dPad['down'] = False

  controlD = calc_dpad_input(dPad)


def render():
  if itsDead:
    return False

  global ticktime
  global display_char
  global output_list
  global uiView
  zap(h)

  pos = (random.randint(0, 100 - w), random.randint(0, 100 - h))
  viewbox = crop(mMap['tiles'], pos)
  uiLayer = None
  if uiView.show_ui:
    uiLayer = uiView.render()
  for y in range(h):
    row = ''
    for x in range(w):
      if uiLayer and uiLayer[y][x] != ' ':
        row += uiLayer[y][x]
      else:
        if viewbox[y][x] == '0':
          row += display_char
        else:
          row += viewbox[y][x]

    output_list[y] = row


def main():
  global itsDead
  global framestart
  global ticktime
  while itsDead == False:
    sleep(0.0001)
    try:
      if time() - framestart > ticktime:
        framestart = time()
        render()
        sys.stdin.flush()

    except (KeyboardInterrupt, SystemExit):
      for thread in threading.enumerate():
        thread.stop()
      os.system('stty echo')
      os.system('tput cnorm')
      os._exit(0)


with output(output_type="list", initial_len=h) as output_list, keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
  init()
  os.system('clear')
  zap(7)
  main()
