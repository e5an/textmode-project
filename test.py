from pynput import keyboard
from reprint import output
from time import time
import sys
import os

framestart = time()
ticktime = .15  # 20fps
h = 20
w = 30

def on_press(key):
  try:
    print('alphanumeric key{0} pressed'.format(key.char))
  except AttributeError:
    print('special key {0} pressed'.format(key))

def on_release(key):
  print('{0}released'.format(key))
  if key == keyboard.Key.esc:
    # Stop listener
    return False

def main():
  for y in range(h):
    row = ''
    for x in range(w):
      row += 'o'
    output_list[y] = row

os.system('clear')

with keyboard.Listener(on_press=on_press, on_release=on_release) as listener:
  with output(output_type="list", initial_len=h) as output_list:
    while 1:
      try:
        if time() > framestart + ticktime:
          framestart = time()
          main()
      except(KeyboardInterrupt, SystemExit):
        sys.exit(0)
